#!/usr/bin/env python

import cgitb
import MySQLdb
import json
import sys
cgitb.enable()

class DB(object):

  def __init__(self):
    self.db = MySQLdb.connect(host = 'localhost', user = 'vodokach', db = 'vodokachka', charset='utf8mb4')
    self.db.unicode_literal.charset = 'utf8'
    self.db.string_decoder.charset = 'utf8'
    self.cur = self.db.cursor()

  def __del__(self):
    self.db.close()

  def _fetchDict(self):
    cols = self.cur.description
    return [{cols[i][0]:col for i, col in enumerate(row)} for row in self.cur.fetchall()]

class GetPeriods(DB):

  def execute(self, ownerId):
    self.cur.execute('select * from periods where waterOwnerId = %s' % ownerId)
    return self._fetchDict()

class UpdateVolume(DB):

  def execute(self, periodId, volume):
    self.cur.execute('update periods set volume = %s where id = %s' % (volume, periodId))
    self.db.commit()
    return {'status': 'OK', 'message': 'Volume has been successfully updated'}

class GetPaidPeriods(DB):

  def execute(self, ownerId, year, month):
    q = 'select surname, \
                name, \
                middlename, \
                areaNum, \
                areaSize, \
                paid, \
                waterUserId \
         from   waterUsers \
         join   paidPeriods \
           on   waterUsers.id = waterUserId \
         join   periods \
           on   periods.id = periodId \
         where  year = %s \
           and  month = %s \
           and  waterOwnerId = %s' % (year, month, ownerId)
    self.cur.execute(q)
    return self._fetchDict()

class UpdatePaidPeriods(DB):

  def execute(self, waterUserId, periodId, paid):
    q = 'update paidPeriods set paid = %s where waterUserId = %s and periodId = %s' % (paid, waterUserId, periodId)
    self.cur.execute(q)
    self.db.commit()
    return {'status': 'OK', 'message': 'Paid information has been successfully updated'}

class GetRate(DB):

  def execute(self, ownerId):
    self._getRate(ownerId)
    return self._fetchDict()

  def _getRate(self, ownerId):
    self.cur.execute('select * from waterOwners where id = %s' % ownerId)

class SetRate(GetRate):

  def execute(self, ownerId, rate):
    if self._isRateSet(ownerId, rate):
      return {'status': 'Fail', 'message': 'Rate already set'}
    else:
      query = 'update waterOwners set rate = %s where id = %s' % (rate, ownerId)
      self.cur.execute(query)
      self.db.commit()
      return {'status': 'OK', 'message': 'Rate has been successfully set'}

  def _isRateSet(self, ownerId, rate):
    self._getRate(ownerId)
    res = self._fetchDict()[0]
    return res['rate']

class RequestFactory(object):

  @staticmethod
  def query(operation, **kargs):
    targetClass = globals()[operation]
    return targetClass().execute(**kargs)

if __name__ == '__main__':
  req = json.load(sys.stdin)
  res = RequestFactory.query(**req)

  print "Content-Type: text/plain;charset=utf-8"
  print

  print json.dumps(res)
