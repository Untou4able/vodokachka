(function() {

  var MenuModel = function() {
    this.pages = {
      'showPeriods': 'views/showPeriods.html',
      'addPeriods': 'views/addPeriods.html',
      'addWaterUsers': 'views/addWaterUsers.html',
    };
    this.defaultPage = 'views/showPeriods.html';
  }

  MenuModel.prototype.getPages = function() {
    return this.pages;
  }

  MenuModel.prototype.getDefaultPage = function() {
    return this.defaultPage;
  }

  var MenuController = function() {}

  MenuController.prototype.init = function() {
    menuView.init();
  }

  MenuController.prototype.getLink = function(linkId) {
    var links = menuModel.getPages();
    return (links[linkId]) ? links[linkId] : menuModel.getDefaultPage();
  }

  var MenuView = function() {
    this.$menu = $('#menu a');
    this.$body = $('#body');
  }

  MenuView.prototype.init = function() {
    this.$menu.bind('click', this.addClickEvent.bind(this));
    this.displayPage();
  }

  MenuView.prototype.addClickEvent = function(e) {
    this.activateMenu(e);
    var link = menuController.getLink(e.target.id);
  }

  MenuView.prototype.activateMenu = function(e) {
    e.preventDefault();
    this.$menu.parent().removeClass("active");
    $(e.target).parent().addClass("active");
    this.displayPage(e.target.id);
  }

  MenuView.prototype.displayPage = function(pageId) {
    var link = menuController.getLink(pageId);
    $.ajax({
      url: link,
      accepts: { html: 'text/html' },
      dataType: 'html',
      context: this,
      async: true
    }).done(function(page) {
      this.$body.html(page);
    });
  }

  var menuModel = new MenuModel();
  var menuView = new MenuView();
  var menuController = new MenuController();
  menuController.init();

})();
