'use strict';
(function() {

/*
  Список расчетных периодов и соответствующуи им данные счетчика загружаются единовременно во время загрузки страницы.
  Пока не уверен, что данные счетчика надо загружать срзау, возможно имеет смысле подгрузать их из базы при изменении периода.

  Два варианта сохранения информации о прлательщиках(кто оплатил, кто нет(галочка)):
    1 - по клику на, собственно, галочку сразу апдэтить базу.
    2 - по кнопке "Сохранить"(#updateVolume) делать большой апдэйт по всем потребителям, вместе с потребленным объемом
  пока попробую первый вариант

*/

/*--------------MODEL--------------*/

var PeriodsModel = function() {
  this.requestFile = 'cgi-bin/periods.py';
  this.ownerId = 1; // Захардкодено
}

PeriodsModel.prototype.request = function(data) {
  return $.ajax({
    url: this.requestFile,
    type: 'POST',
    accepts: { json: 'application/json' },
    dataType: 'json',
    data: JSON.stringify(data)
  });
}

PeriodsModel.prototype.getPeriods = function() {
  var data = {
    'operation': 'GetPeriods',
    'ownerId': this.ownerId
  }
  return this.request(data);
}

PeriodsModel.prototype.getPaidPeriods = function(year, month) {
  var data = {
    'operation': 'GetPaidPeriods',
    'ownerId': this.ownerId,
    'year': year,
    'month': month
  };
  return this.request(data);
}

PeriodsModel.prototype.getRate = function() {
  var data = {
    'operation': 'GetRate',
    'ownerId': this.ownerId
  };
  return this.request(data);
}

PeriodsModel.prototype.setRate = function(rate) {
  var data = {
    'operation': 'SetRate',
    'ownerId': this.ownerId,
    'rate': rate
  };
  return this.request(data);
}

PeriodsModel.prototype.setVolume = function(periodId, volume) {
  var data = {
    'operation': 'UpdateVolume',
    'periodId': periodId,
    'volume': volume
  };
  return this.request(data);
}

PeriodsModel.prototype.setWaterUserPaid = function(waterUserId, periodId, paid) {
  var data = {
    'operation': 'UpdatePaidPeriods',
    'waterUserId': waterUserId,
    'periodId': periodId,
    'paid': paid
  };
  return this.request(data);
}

/*-------------CONTROLLER------------*/

var PeriodsController = function() {}

PeriodsController.prototype.init = function() {
  periodsView.init();
}

PeriodsController.prototype.getPeriods = function() {
  var periods = periodsModel.getPeriods();
  return periods.then(function(periods) {
    periods = periods.sort(this.comparePeriods);
    return this.makeYearsToMonthsHash(periods);
  }.bind(this));
}

PeriodsController.prototype.comparePeriods = function(a, b) {
  var period1 = new Date();
  var period2 = new Date();
  period1.setYear(a['year']);
  period1.setMonth(a['month']);
  period2.setYear(b['year']);
  period2.setMonth(b['month']);
  return period1 - period2;
}

PeriodsController.prototype.getPaidPeriods = function(year, month) {
  return periodsModel.getPaidPeriods(parseInt(year), parseInt(month));
}

PeriodsController.prototype.makeYearsToMonthsHash = function(periods) { //похоже надо поменять имя метода
  var yearsToMonthsHash = {};
  for(var i in periods) {
    var year = periods[i]['year'];
    var month = periods[i]['month'];
    var volume = periods[i]['volume'];
    var id = periods[i]['id'];
    if(!(year in yearsToMonthsHash))
      yearsToMonthsHash[year] = {};
    yearsToMonthsHash[year][month] = {'volume': volume, 'periodId': id};
  }
  return yearsToMonthsHash;
}

PeriodsController.prototype.getRate = function() {
  return periodsModel.getRate().then(function(rate) {
    return rate[0];
  });
}

PeriodsController.prototype.setRate = function(rate) {
  rate = rate.replace(/\ /g, '').replace(',', '.');
  return periodsModel.setRate(parseFloat(rate));
}

PeriodsController.prototype.setVolume = function(periodId, volume) {
  return periodsModel.setVolume(parseInt(periodId), parseInt(volume));
}

PeriodsController.prototype.setWaterUserPaid = function(waterUserId, periodId, paid) {
  return periodsModel.setWaterUserPaid(parseInt(waterUserId), parseInt(periodId), paid);
}

PeriodsController.prototype.calculateDebts = function(users, rate, volume) {
  var totalArea = 0;
  for(var i in users)
    totalArea += parseInt(users[i]['areaSize']);
  for(var i in users)
    users[i]['debt'] = (parseInt(rate) * parseInt(volume)) * (users[i]['areaSize'] / totalArea);
  return users;
}

/*------------VIEW--------------*/

var PeriodsView = function() {
  this.monthNumberToName = {
    1: 'Январь',
    2: 'Февраль',
    3: 'Март',
    4: 'Апрель',
    5: 'Май',
    6: 'Июнь',
    7: 'Июль',
    8: 'Август',
    9: 'Сентябрь',
    10: 'Октябрь',
    11: 'Ноябрь',
    12: 'Декабрь',
  };
  this.yearSelect = document.getElementById('year');
  this.monthSelect = document.getElementById('month');
  this.volumeInput = document.getElementById('volume');
  this.volumeBtn = document.getElementById('updateVolume');
  this.waterUsersTable = document.getElementById('waterUsers').getElementsByTagName('tbody')[0];
  this.rateInput = document.getElementById('rateAmount');
  this.rateBtn = document.getElementById('setRate');
}

PeriodsView.prototype.year = function() {
  return this.yearSelect.value;
}

PeriodsView.prototype.month = function() {
  return this.monthSelect.value;
}

PeriodsView.prototype.volume = function() {
  return this.volumeInput.value;
}

PeriodsView.prototype.rate = function() {
  return this.rateInput.value;
}

PeriodsView.prototype.init = function() {
  this.initPeriods();
  this.initRate();
  this.bindEvents();
}

PeriodsView.prototype.initPeriods = function() {
  var periodsData = periodsController.getPeriods();
  periodsData.then(function(periods) {
    this.periods = periods;
    this.fillPeriods();
    this.setVolumeOnPeriodChange();
    this.fillWaterUsers();
  }.bind(this));
}

PeriodsView.prototype.fillPeriods = function() {
  var lastYear = this.fillYears();
  this.fillMonths(lastYear);
}

PeriodsView.prototype.fillYears = function() {
  this.clearSelect(this.yearSelect);
  for(var year in this.periods) {
    var yearOption = document.createElement('option');
    yearOption.value = year;
    yearOption.text = year;
    this.yearSelect.add(yearOption);
  }
  this.yearSelect.value = year;
  return year;
}

PeriodsView.prototype.clearSelect = function(select) {
  for(var _ in select)
    select.options.remove(0);
}

PeriodsView.prototype.fillMonths = function(year) {
  this.clearSelect(this.monthSelect);
  for(var month in this.periods[year]) {
    var monthOption = document.createElement('option');
    monthOption.value = month;
    monthOption.text = this.monthNumberToName[month];
    this.monthSelect.add(monthOption);
  }
  this.monthSelect.value = month;
}

PeriodsView.prototype.fillWaterUsers = function() {
  var year = this.yearSelect.value;
  var month = this.monthSelect.value;
  this.waterUsersTable.innerHTML = "";
  periodsController.getPaidPeriods(year, month).then(function(waterUsers) {
    var calcedWaterUsers = periodsController.calculateDebts(waterUsers, this.rate(), this.volume());
    for(var i in waterUsers)
      this.insertWaterUser(waterUsers[i]);
  }.bind(this));
}

PeriodsView.prototype.insertWaterUser = function(waterUser) { //alarm! big ugly method
  var row = this.waterUsersTable.insertRow(this.waterUsersTable.length);
  var nameCell = row.insertCell(0);
  nameCell.innerText = this.makeDisplayableName(waterUser['surname'], waterUser['name'], waterUser['middlename']);
  var areaNumCell = row.insertCell(1);
  areaNumCell.innerText = waterUser['areaNum'];
  var areaSizeCell = row.insertCell(2);
  areaSizeCell.innerText = waterUser['areaSize'];
  var debtCell = row.insertCell(3);
  debtCell.innerText = waterUser['debt'];
  var isPaidCell = row.insertCell(4);
  isPaidCell.appendChild(this.makePaidCheckBox(waterUser['waterUserId'], waterUser['paid']));
}

PeriodsView.prototype.makeDisplayableName = function(surname, name, middlename) {
  return surname + ' ' + name[0] + '.' + middlename[0] + '.';
}

PeriodsView.prototype.makePaidCheckBox = function(waterUserId, paid) {
  var checkbox = document.createElement('input');
  checkbox.type = 'checkbox';
  checkbox.value = waterUserId;
  checkbox.checked = paid;
  checkbox.addEventListener('click', this.onPaidCheckboxClicked.bind(this));
  return checkbox;
}

PeriodsView.prototype.initRate = function() {
  periodsController.getRate().then(function(rate) {
    if(!rate['rate']) {
      this.rateInput.readOnly = false;
      $(this.rateBtn).removeClass('sr-only');
    } else {
      this.rateInput.value = rate['rate'];
    }
  }.bind(this));
}

PeriodsView.prototype.bindEvents = function() {
  this.yearSelect.addEventListener('change', this.refillMonthsOnYearChange.bind(this));
  this.yearSelect.addEventListener('change', this.setVolumeOnPeriodChange.bind(this));
  this.yearSelect.addEventListener('change', this.fillWaterUsers.bind(this));
  this.monthSelect.addEventListener('change', this.setVolumeOnPeriodChange.bind(this));
  this.monthSelect.addEventListener('change', this.fillWaterUsers.bind(this));
  this.rateBtn.addEventListener('click', this.onRateBtnClicked.bind(this));
  this.volumeBtn.addEventListener('click', this.onUpdateVolumeBtnClicked.bind(this));
}

PeriodsView.prototype.refillMonthsOnYearChange = function(e) {
  var year = e.target.value;
  this.fillMonths(year);
}

PeriodsView.prototype.setVolumeOnPeriodChange = function(e) {
  this.volumeInput.value = this.periods[this.year()][this.month()]['volume'];
}

PeriodsView.prototype.onRateBtnClicked = function(e) {
  var btnClicked = e.target;
  btnClicked.disabled = true;
  periodsController.setRate(this.rate()).then(function(res) {
    if(res['status'] == 'OK') {
      this.rateInput.disabled = true;
      $(btnClicked).addClass('disabled');
    }
  }.bind(this));
}

PeriodsView.prototype.onUpdateVolumeBtnClicked = function(e) {
  var periodId = this.periods[this.year()][this.month()]['periodId'];
  periodsController.setVolume(periodId, this.volume()).then(function(res) {
    console.log(res);
  });
}

PeriodsView.prototype.onPaidCheckboxClicked = function(e) {
  var waterUserId = e.target.value;
  var paid = e.target.checked;
  var periodId = this.periods[this.year()][this.month()]['periodId'];
  periodsController.setWaterUserPaid(waterUserId, periodId, paid).then(function(res) {
    console.log(res);
  });
}

var periodsModel = new PeriodsModel();
var periodsView = new PeriodsView();
var periodsController = new PeriodsController();
periodsController.init();
})();
